const express = require("express");

const {
    postAOrderOfCustomer,
    getAllOrderOfCustomer,
    getAOrderById,
    putAOrderById,
    deleteAOrderById
} = require("../controller/orderController");

const orderRouter = express.Router();

orderRouter.get("/customer/:customerId/order", getAllOrderOfCustomer);
orderRouter.post("/customer/:customerId/order", postAOrderOfCustomer);
orderRouter.get("/order/:orderId", getAOrderById);
orderRouter.put("/order/:orderId", putAOrderById);
orderRouter.delete("/customer/:customerId/order/:orderId", deleteAOrderById);

module.exports = {orderRouter};
