const express = require("express");

const {
    postAProduct,
    getAllProduct,
    getAProductById,
    putAProductById,
    deleteAProductById
} = require("../controller/productController");

const productRouter = express.Router();

productRouter.get("/product", getAllProduct);
productRouter.post("/product", postAProduct);
productRouter.get("/product/:id", getAProductById);
productRouter.put("/product/:id", putAProductById);
productRouter.delete("/product/:id", deleteAProductById);

module.exports = {productRouter};
