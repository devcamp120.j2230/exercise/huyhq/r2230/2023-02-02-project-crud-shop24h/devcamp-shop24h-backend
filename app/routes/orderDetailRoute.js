const express = require("express");

const {
    postAOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getAOrderDetailById,
    putAOrderDetailById,
    deleteAOrderDetailById
} = require("../controller/orderDetailController");

const orderDetailRouter = express.Router();

orderDetailRouter.get("/order/:orderId/order-detail", getAllOrderDetailOfOrder);
orderDetailRouter.post("/order/:orderId/order-detail", postAOrderDetailOfOrder);
orderDetailRouter.get("/order-detail/:id", getAOrderDetailById);
orderDetailRouter.put("/order-detail/:id", putAOrderDetailById);
orderDetailRouter.delete("/order/:orderId/order-detail/:id", deleteAOrderDetailById);

module.exports = {orderDetailRouter};
