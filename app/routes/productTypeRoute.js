const express = require("express");

const {
    postAProductType,
    getAllProductType,
    getAProductTypeById,
    putAProductTypeById,
    deleteAProductTypeById
} = require("../controller/productTypeController");

const productTypeRouter = express.Router();

productTypeRouter.get("/product-type", getAllProductType);
productTypeRouter.post("/product-type", postAProductType);
productTypeRouter.get("/product-type/:id", getAProductTypeById);
productTypeRouter.put("/product-type/:id", putAProductTypeById);
productTypeRouter.delete("/product-type/:id", deleteAProductTypeById);

module.exports = {productTypeRouter};
