const express = require("express");

const {
    postACustomer,
    getAllCustomer,
    getACustomerById,
    putACustomerById,
    deleteACustomerById
} = require("../controller/customerController");

const customerRouter = express.Router();

customerRouter.get("/customer", getAllCustomer);
customerRouter.post("/customer", postACustomer);
customerRouter.get("/customer/:id", getACustomerById);
customerRouter.put("/customer/:id", putACustomerById);
customerRouter.delete("/customer/:id", deleteACustomerById);

module.exports = {customerRouter};
