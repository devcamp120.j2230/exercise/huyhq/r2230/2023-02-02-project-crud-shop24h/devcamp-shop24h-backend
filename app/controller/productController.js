const { default: mongoose } = require("mongoose");

const productModel = require("../model/Product");

const postAProduct = (req, res) => {
    var body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: "Loi 400: Ten san pham phai bat buoc."
        })
    };

    if (!body.type) {
        return res.status(400).json({
            message: "Loi 400: Id loai san pham phai bat buoc."
        })
    };

    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            message: "Loi 400: Id loai san pham khong dung."
        })
    };

    if (!body.imageUrl) {
        return res.status(400).json({
            message: "Loi 400: imageUrl phai bat buoc."
        })
    };

    if (!body.imageUrl) {
        return res.status(400).json({
            message: "Loi 400: Anh Url phai bat buoc."
        })
    };

    if (!body.buyPrice) {
        return res.status(400).json({
            message: "Loi 400: Gia san pham phai bat buoc."
        })
    };

    if (!Number.isInteger(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            message: "Loi 400: Gia san pham khong dung."
        })
    };

    if (!body.promotionPrice) {
        return res.status(400).json({
            message: "Loi 400: Gia khuyen mai phai bat buoc."
        })
    };

    if (!Number.isInteger(body.promotionPrice) || body.promotionPrice < 0) {
        return res.status(400).json({
            message: "Loi 400: Gia khuyen mai khong dung."
        })
    };

    if (!Number.isInteger(body.amount) || body.amount < 0) {
        return res.status(400).json({
            message: "Loi 400: So luong khong dung."
        })
    };

    var newProduct = new productModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    })

    productModel.create(newProduct, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(201).json({
                message: `Tao moi san pham thanh cong.`,
                product: data
            });
        };
    });
};

const getAllProduct = (req, res) => {
    productModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay san pham thanh cong.`,
                product: data
            });
        };
    })
};

const getAProductById = (req, res) => {
    var id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id san pham khong dung."
        })
    }

    productModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay san pham thanh cong.`,
                product: data
            });
        };
    })
};

const putAProductById = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id san pham khong dung."
        })
    };

    var body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: "Loi 400: Ten san pham phai bat buoc."
        })
    };

    if (!body.type) {
        return res.status(400).json({
            message: "Loi 400: Id loai san pham phai bat buoc."
        })
    };

    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            message: "Loi 400: Id loai san pham khong dung."
        })
    };

    if (!body.imageUrl) {
        return res.status(400).json({
            message: "Loi 400: imageUrl phai bat buoc."
        })
    };

    if (!body.imageUrl) {
        return res.status(400).json({
            message: "Loi 400: Anh Url phai bat buoc."
        })
    };

    if (!body.buyPrice) {
        return res.status(400).json({
            message: "Loi 400: Gia san pham phai bat buoc."
        })
    };

    if (!Number.isInteger(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            message: "Loi 400: Gia san pham khong dung."
        })
    };

    if (!body.promotionPrice) {
        return res.status(400).json({
            message: "Loi 400: Gia khuyen mai phai bat buoc."
        })
    };

    if (!Number.isInteger(body.promotionPrice) || body.promotionPrice < 0) {
        return res.status(400).json({
            message: "Loi 400: Gia khuyen mai khong dung."
        })
    };

    if (!Number.isInteger(body.amount) || body.amount < 0) {
        return res.status(400).json({
            message: "Loi 400: So luong khong dung."
        })
    };

    var product = new productModel({
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    })

    productModel.findByIdAndUpdate(id, product, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua san pham thanh cong.`,
                product: data
            });
        };
    });
};

const deleteAProductById = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id san pham khong dung."
        })
    };
    productModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(204).json({
                message: `Xoa san pham thanh cong.`,
                product: data
            });
        };
    });
};

module.exports = {
    postAProduct,
    getAllProduct,
    getAProductById,
    putAProductById,
    deleteAProductById
};
