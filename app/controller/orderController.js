const { default: mongoose } = require("mongoose");

const orderModel = require("../model/Order");
const customerModel = require("../model/Customer");

const postAOrderOfCustomer = (req, res) => {
    var customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Loi 400: Id Customer khong dung."
        })
    }

    var body = req.body;

    var newOrder = new orderModel({
        _id: mongoose.Types.ObjectId(),
        shippedDate: body.shippedDate,
        note: body.note,
    })

    orderModel.create(newOrder, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            customerModel.findByIdAndUpdate(customerId,
                {
                    $push: { order: data._id }
                },
                (err) => {
                    if (err) {
                        return res.status(500).json({
                            message: `Loi 500: ${error.message}.`
                        });
                    } else {
                        return res.status(201).json({
                            message: `Tao don hang moi thanh cong.`,
                            Order: data
                        });
                    }
                }
            )

        };
    });
};

const getAllOrderOfCustomer = (req, res) => {
    var customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Loi 400: Id Customer khong dung."
        })
    };

    customerModel.findById(customerId)
        .populate("order")
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                return res.status(200).json({
                    message: `Lay thong tin thanh cong.`,
                    order: data.order
                });
            };
        })
};

const getAOrderById = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    }

    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay thong tin thanh cong.`,
                Order: data
            });
        };
    })
};

const putAOrderById = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    }

    var body = req.body;

    var order = new orderModel({
        shippedDate: body.shippedDate,
        note: body.note,
    })

    orderModel.findByIdAndUpdate(orderId, order, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua thong tin thanh cong.`,
                Order: data
            });
        };
    });
};

const deleteAOrderById = (req, res) => {
    var customerId = req.params.customerId;

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Loi 400: Id Customer khong dung."
        })
    };

    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    }

    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            customerModel.findByIdAndUpdate(customerId,
                {
                    $pull: { order: orderId }
                },
                (err) => {
                    if (err) {

                    } else {
                        return res.status(204).json({
                            message: `Xoa Order thanh cong.`,
                            Order: data
                        });
                    }
                }
            )
        };
    });
};

module.exports = {
    postAOrderOfCustomer,
    getAllOrderOfCustomer,
    getAOrderById,
    putAOrderById,
    deleteAOrderById
};
