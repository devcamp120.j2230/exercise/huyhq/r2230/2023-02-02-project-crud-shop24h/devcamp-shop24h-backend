const { default: mongoose } = require("mongoose");

const orderModel = require("../model/Order");
const orderDetailModel = require("../model/OrderDetail");

const postAOrderDetailOfOrder = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    }

    var body = req.body;

    if (!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            message: "Loi 400: Id Product khong dung dinh dang."
        })
    }

    if (!Number.isInteger(body.quantity) || body.quantity < 0) {
        return res.status(400).json({
            message: "Loi 400: Quantity khong dung dinh dang."
        })
    }

    var newOrderDetail = new orderDetailModel({
        _id: mongoose.Types.ObjectId(),
        product: body.product,
        quantity: body.quantity,
    })

    orderDetailModel.create(newOrderDetail, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            orderModel.findByIdAndUpdate(orderId,
                {
                    $push: { orderDetail: data._id }
                },
                (err) => {
                    if (err) {
                        return res.status(500).json({
                            message: `Loi 500: ${error.message}.`
                        });
                    } else {
                        return res.status(201).json({
                            message: `Tao chi tiet don hang moi thanh cong.`,
                            orderDetail: data
                        });
                    }
                }
            )

        };
    });
};

const getAllOrderDetailOfOrder = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    };

    orderModel.findById(orderId)
        .populate("orderDetail")
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: `Loi 500: ${error.message}.`
                });
            } else {
                return res.status(200).json({
                    message: `Lay thong tin thanh cong.`,
                    orderDetail: data.orderDetail
                });
            };
        })
};

const getAOrderDetailById = (req, res) => {
    var id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id OrderDetail khong dung."
        })
    }

    orderDetailModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay thong tin thanh cong.`,
                orderDetail: data
            });
        };
    })
};

const putAOrderDetailById = (req, res) => {
    var id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id OrderDetail khong dung."
        })
    }

    var body = req.body;

    if (!mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            message: "Loi 400: Id Product khong dung dinh dang."
        })
    }

    if (!Number.isInteger(body.quantity) || body.quantity < 0) {
        return res.status(400).json({
            message: "Loi 400: Quantity khong dung dinh dang."
        })
    }

    var orderDetail = new orderDetailModel({
        product: body.product,
        quantity: body.quantity,
    })

    orderDetailModel.findByIdAndUpdate(id, orderDetail, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua thong tin thanh cong.`,
                orderDetail: data
            });
        };
    });
};

const deleteAOrderDetailById = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "Loi 400: Id Order khong dung."
        })
    };

    var orderDetailId = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            message: "Loi 400: Id OrderDetail khong dung."
        })
    };

    orderDetailModel.findByIdAndDelete(orderDetailId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            orderModel.findByIdAndUpdate(orderId,
                {
                    $pull: { orderDetail: orderDetailId }
                },
                (err) => {
                    if (err) {

                    } else {
                        return res.status(204).json({
                            message: `Xoa OrderDetail thanh cong.`,
                            orderDetail: data
                        });
                    }
                }
            )
        };
    });
};

module.exports = {
    postAOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getAOrderDetailById,
    putAOrderDetailById,
    deleteAOrderDetailById
};
