const { default: mongoose } = require("mongoose");

const customerModel = require("../model/Customer");

const postACustomer = (req, res) => {
    var body = req.body;
    if (!body.fullName) {
        return res.status(400).json({
            message: "Loi 400: Ten khach hang phai bat buoc."
        })
    };

    if (!body.phone) {
        return res.status(400).json({
            message: "Loi 400: So dien thoai phai bat buoc."
        })
    };

    if (!body.email) {
        return res.status(400).json({
            message: "Loi 400: Email phai bat buoc."
        })
    };

    var newCustomer = new customerModel({
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
    })

    customerModel.create(newCustomer, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(201).json({
                message: `Tao khach hang moi thanh cong.`,
                Customer: data
            });
        };
    });
};

const getAllCustomer = (req, res) => {
    customerModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay thong tin thanh cong.`,
                Customer: data
            });
        };
    })
};

const getACustomerById = (req, res) => {
    var id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    }

    customerModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Lay thong tin thanh cong.`,
                Customer: data
            });
        };
    })
};

const putACustomerById = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    };

    var body = req.body;
    if (!body.fullName) {
        return res.status(400).json({
            message: "Loi 400: Ten khach hang phai bat buoc."
        })
    };

    if (!body.phone) {
        return res.status(400).json({
            message: "Loi 400: So dien thoai phai bat buoc."
        })
    };

    if (!body.email) {
        return res.status(400).json({
            message: "Loi 400: Email phai bat buoc."
        })
    };

    var customer = new customerModel({
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
    })

    customerModel.findByIdAndUpdate(id, customer, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(200).json({
                message: `Sua thong tin thanh cong.`,
                Customer: data
            });
        };
    });
};

const deleteACustomerById = (req, res) => {
    var id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: "Loi 400: Id khach hang khong dung."
        })
    };
    customerModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Loi 500: ${error.message}.`
            });
        } else {
            return res.status(204).json({
                message: `Xoa khach hang thanh cong.`,
                Customer: data
            });
        };
    });
};

module.exports = {
    postACustomer,
    getAllCustomer,
    getACustomerById,
    putACustomerById,
    deleteACustomerById
};
