const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: Date,
    },
    note: String,
    orderDetail: [{
        type: mongoose.Types.ObjectId,
        ref: "orderDetail",
    }],
});

module.exports = mongoose.model("order", orderSchema)