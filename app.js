const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());

//get model
const productTypeModel = require("./app/model/ProductType");
const productModel = require("./app/model/Product");
const customerModel = require("./app/model/Customer");
const orderModel = require("./app/model/Order");
const orderDetailModel = require("./app/model/OrderDetail");
//get route
const {productTypeRouter} = require("./app/routes/productTypeRoute");
const {productRouter} = require("./app/routes/productRoute");
const {customerRouter} = require("./app/routes/customerRoute");
const {orderRouter} = require("./app/routes/orderRoute");
const {orderDetailRouter} = require("./app/routes/orderDetailRoute");

//connect mongoDb
const nameDB = "CRUD_Shop24h";
mongoose.connect("mongodb://127.0.0.1:27017/"+nameDB, (error)=>{
    if (error) throw error;
    console.log('Successfully connected to DB: ' + nameDB);
})

const port = 8000;
app.get("/", (req, res)=>{
    return res.status(200).json({
        message: "Connect success!"
    })
})

app.use("/", productTypeRouter);
app.use("/", productRouter);
app.use("/", customerRouter);
app.use("/", orderRouter);
app.use("/", orderDetailRouter);

app.listen(port, ()=>{
    console.log("Connect to port: "+port);
})

